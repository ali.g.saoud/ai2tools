from dotenv import find_dotenv, load_dotenv
from langchain.llms import OpenAI
from langchain import PromptTemplate
from langchain.chains import LLMChain
from langchain.agents import load_tools
from langchain.agents import initialize_agent
from langchain.agents import AgentType
from langchain.agents.load_tools import get_all_tool_names
from langchain import ConversationChain
from flask import Flask

app = Flask(__name__)

@app.route("/members")
def _():
    return {"members": ['hi', 'ayee']}

load_dotenv(find_dotenv())

def getResponse (text):

    llm = OpenAI(temperature=1)
    conversation = ConversationChain(llm=llm, verbose=True)
    
    return conversation.predict(
        input=conversation.predict(
            input = text
        )
    )
    
if __name__ == '__main__':
    app.run(debug=True)
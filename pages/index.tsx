import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { useRef, useState } from "react";
import { Toaster, toast } from "react-hot-toast";
import DropDown, { VibeType } from "../components/DropDown";
import Footer from "../components/Footer";
import Github from "../components/GitHub";
import Header from "../components/Header";
import LoadingDots from "../components/LoadingDots";

import {
  createParser,
  ParsedEvent,
  ReconnectInterval,
} from "eventsource-parser";
import { stringify } from "querystring";

const Home: NextPage = () => {
  const [loading, setLoading] = useState(false);
  const [bio, setBio] = useState("");
  const [vibe, setVibe] = useState<VibeType>("Professional");
  const [generatedBios, setGeneratedBios] = useState<String>("");

  const bioRef = useRef<null | HTMLDivElement>(null);

  const scrollToBios = () => {
    if (bioRef.current !== null) {
      bioRef.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  const prompt = `Generate a setence that sounds like it comes from a sentient being
      Make sure that the sentence is super related to the following question: ${bio}`;

  const generateBio = async (e: any) => {
    e.preventDefault();
    setGeneratedBios("");
    setLoading(true);

    var response = await fetch("http://localhost:3001/generatep", {
      method: "POST",
      body: JSON.stringify({
          prompt: bio,
      }),
      headers: {
          "Content-type": "application/json; charset=UTF-8"
      }
    });

    var r = await response.json()
    
    console.log("hi")
    console.log(r)
    console.log(r['response'])

    setGeneratedBios((prev) => r['response']);
    scrollToBios();
    setLoading(false);
  };

  return (
    <div className="flex max-w-5xl mx-auto flex-col items-center justify-center py-2 min-h-screen">
      <Head>
        <title>AI2Tools</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />
      <main className="flex flex-1 w-full flex-col items-center justify-center text-center px-4 mt-12 sm:mt-20">
        
        <h1 className="sm:text-6xl text-4xl max-w-[708px] font-bold text-white">
          AI2Tools
        </h1>
        <p className="text-slate-500 mt-5">AI2Tools thinks alone sometimes, browses the internet on desire, and has continous long term memory</p>
        <div className="max-w-xl w-full">
          <div className="flex mt-10 items-center space-x-3">

          
          </div>
          <textarea
            value={bio}
            onChange={(e) => setBio(e.target.value)}
            rows={2}
            className="w-full rounded-md border-gray-300 shadow-sm focus:border-black focus:ring-black my-5"
            placeholder={
              "Did you know that you're a really amazing entity?"
            }
          />

          {!loading && (
            <button
              className="bg-gray-700 rounded-xl text-white font-medium px-4 py-2 sm:mt-10 mt-8 hover:bg-gray-400 w-full"
              onClick={(e) => generateBio(e)}
            >
              Talk
            </button>
          )}
          {loading && (
            <button
              className="bg-gray-700 rounded-xl text-white font-medium px-4 py-2 sm:mt-10 mt-8 hover:bg-gray-400 w-full"
              disabled
            >
              <LoadingDots color="white" style="large" />
            </button>
          )}
        </div>
        <Toaster
          position="top-center"
          reverseOrder={false}
          toastOptions={{ duration: 2000 }}
        />
        <hr className="h-px bg-gray-700 border-1 dark:bg-gray-700" />
        <div className="space-y-10 my-10">
          {generatedBios && (
            <>
              <div>
                <h2
                  className="sm:text-4xl text-3xl font-bold text-white mx-auto"
                  ref={bioRef}
                >
                  StrAnGI:
                </h2>
              </div>
              <div className="space-y-8 flex flex-col items-center justify-center max-w-xl mx-auto">
              <div
                        className="bg-white rounded-xl shadow-md p-4 hover:bg-gray-100 transition cursor-copy border"
                        onClick={() => {
                          navigator.clipboard.writeText(generatedBios.toString());
                          toast("Response copied.", {
                            icon: "🗣️",
                          });
                        }}
                      >
                        <p>{generatedBios}</p>
                      </div>
              </div>
            </>
          )}
        </div>
      </main>
      <Footer />
    </div>
  );
};

export default Home;

import os

from typing import Dict
from fastapi import FastAPI, Request, Response
from starlette.responses import StreamingResponse
import openai
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
from pydantic import BaseModel
import json as json
import openai
import dotenv
import pinecone
from uuid import uuid4
from pyhelpers.store import save_json

dotenv.load_dotenv()


pinecone.init(api_key='a0a95cf2-546f-42dc-b8c6-ef95a5e88ab0', environment='asia-southeast1-gcp-free')
openai.api_key ='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'

index = pinecone.Index('ai2tools-mvp')

class Req(BaseModel):
  prompt: str

config = {
    "runtime": "edge"
}


app = FastAPI()

# define the allowed origins
origins = [
  "http://localhost:3000",
  "http://localhost:3001",
]

# add the middleware to the app
app.add_middleware(
  CORSMiddleware,
  allow_origins=origins,
  allow_credentials=True,
  allow_methods=["*"],
  allow_headers=["*"],
)

@app.get("/generatep")
@app.post("/generatep")
async def generatep(req: Req) -> Response:
  prompt=req.prompt
  vector = gpt3_embedding(prompt)
  payload = list()
  unique_id = str(uuid4())
  metadata = {
    'speaker': 'user',
    'message': prompt,
    'uuid': unique_id
  }
  
  payload.append((unique_id, vector, metadata))
  
  results = index.query(vector=vector, top_k=3, include_values=False, include_metadata=True)

  importants = load_important_metadata(results)

  memory=chat_to_memory(importants)
  print('memory is %s'%memory)
  _ = openai.Completion.create(
     model='text-davinci-003',
     prompt='Your name is assistant, and only that, no openai, no gpt. The following is your memory of a previous conversation:\n'+memory+'\n'+'now answer this question: user: '+prompt,
     max_tokens=250
  ) ['choices'][0].text.strip()
  
  unique_id = str(uuid4())
  metadata = {
    'speaker': 'assistant',
    'message': _,
    'uuid': unique_id
  }
  payload.append((unique_id, vector, metadata))
  index.upsert(payload)
  print(_)
  rr_json = json.dumps(_)
  print('{"response": '+rr_json+'}')
  print(f'rr: {_}')
  return Response('{"response": '+rr_json+'}', status_code=200)

async def __(req: Req) -> Response:

    print(f'heyy {req.prompt}')
    print (req)

    rr = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{'role': 'user', 'content': req.prompt}],
        max_tokens=250,
    )['choices'][0].message.content.strip()
    # use json.dumps() to escape the control characters in rr
    rr_json = json.dumps(rr)
    print('{"response": '+rr_json+'}')
    print(f'rr: {rr}')
    return Response('{"response": '+rr_json+'}', status_code=200)

def chat_to_memory(results):
  strr=''
  for i in results:
    strr = strr + '-role: ' + i['role'] + ': ' + i['content'] + '\n'
  return strr

def gpt3_embedding(content, engine='text-embedding-ada-002'):
  content=content.encode(errors='ignore', encoding='ASCII').decode() # fix any unicode errors
  return openai.Embedding.create(input=content, engine=engine)['data'][0]['embedding']

def load_important_metadata(results):
  chatlist=[]
  for i in results['matches']:
      chatlist.append( {
        'role': str(i['metadata']['speaker']),
        'content': str(i['metadata']['message'])
      })
  print(chatlist)
  return chatlist

uvicorn.run(app, host='localhost', port=3001)
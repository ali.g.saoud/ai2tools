import Image from "next/image";
import Link from "next/link";

export default function Header() {
  return (
    <header className="flex justify-between items-center w-full mt-5 border-b-2 pb-7 sm:px-4 px-2">
      
      <a
        href="https://vercel.com/templates/next.js/twitter-bio"
        target="_blank"
        rel="noreferrer"
      >
      </a>
      <Link href="/" className="flex space-x-3">
        <h1 className="sm:text-4xl text-2xl ml-2 tracking-tight text-white">
        AI2Tools
        </h1>
      </Link>
      
      <a
        href="https://vercel.com/templates/next.js/twitter-bio"
        target="_blank"
        rel="noreferrer"
      >
      </a>
      <Link href="/" className="flex space-x-3">
        <p className="sm:text-4xl text-2xl ml-2 tracking-tight text-white">
        List
        </p>
      </Link>
      
      <a
        href="https://vercel.com/templates/next.js/twitter-bio"
        target="_blank"
        rel="noreferrer"
      >
      </a>
      <Link href="/" className="flex space-x-3">
        <p className="sm:text-4xl text-2xl ml-2 tracking-tight text-white">
        Me
        </p>
      </Link>

      
      <a
        href="https://vercel.com/templates/next.js/twitter-bio"
        target="_blank"
        rel="noreferrer"
      >
      </a>
    </header>
  );
}
